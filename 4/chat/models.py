from django.db import models

# Create your models here.

from django.db.models import Model as PreModel

ONLINE = 1
OFFLINE = 0

class User(PreModel):
	username = models.CharField(max_length = 100)
	id = models.AutoField(primary_key = True)
	online = models.IntegerField(null = True)
	lastTime = models.FloatField()
	def __str__(self):
		return 'User %s id:%d online:%d lastTime:%f' % (self.username, self.id, self.online, self.lastTime)

class Message(PreModel):
	id = models.AutoField(primary_key = True)
	user = models.ForeignKey(User, on_delete = models.CASCADE)
	message = models.CharField(max_length = 1000)
	postTime = models.FloatField()
	def __str__(self):
		return '%d %s' % (self.id, self.message)

class Session(PreModel):
	id = models.AutoField(primary_key = True)
	user = models.ForeignKey(User, on_delete = models.CASCADE)
	def __str__(self):
		return '%d user:%d' % (self.id, self.user_id)
