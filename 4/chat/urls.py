from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	url(r'^$', views.main),
	url(r'file/', views.file),
	url(r'login$', views.login),
	url(r'logout$', views.logout),
	url(r'users$', views.users),
	path(r'users/<int:other_user_id>', views.user),
	url(r'messages$', views.messages)
]
