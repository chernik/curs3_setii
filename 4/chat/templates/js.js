function sendPost(url, obj, onResp, onErr, token){
	var req = new XMLHttpRequest();
	req.open('POST', url, true);
	req.setRequestHeader('Content-Type', 'application/json');
	if(token !== undefined){
		req.setRequestHeader('Authorization', token);
	}
	req.timeout = 30000;
	var time = new Date().getTime();
	req.onreadystatechange = function(){
		if(req.readyState != 4){
			return;
		}
		time = new Date().getTime() - time;
		if(req.status != 200){
			onErr({
				type: 'connect',
				code: req.status,
				text: req.statusText
			});
			return;
		}
		var res;
		try {
			res = JSON.parse(req.responseText);
		} catch(err){
			onErr({
				type: 'json',
				err: err,
				text: '' + err
			});
			return;
		}
		onResp(res);
	}
	req.send(JSON.stringify(obj));
}
function sendGet(url, obj, onResp, onErr, token){
	function toQuery(obj){
		var s = [];
		for(key in obj){
			s.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]));
		}
		s = s.join('&');
		return s;
	}
	var req = new XMLHttpRequest();
	req.open('GET', url + '?' + toQuery(obj), true);
	if(token !== undefined){
		req.setRequestHeader('Authorization', token);
	}
	var time = new Date().getTime();
	req.onreadystatechange = function(){
		if(req.readyState != 4){
			return;
		}
		time = new Date().getTime() - time;
		if(req.status != 200){
			onErr({
				type: 'connect',
				code: req.status,
				text: req.statusText
			});
			return;
		}
		var res;
		try {
			res = JSON.parse(req.responseText);
		} catch(err){
			onErr({
				type: 'json',
				err: err,
				text: '' + err
			});
			return;
		}
		onResp(res);
	}
	req.send();
}