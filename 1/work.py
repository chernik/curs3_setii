import req
import recv
import genid
import params

import time
import threading

class Work:
	def __enter__(self):
		self.mutex.acquire()
		return self
	
	def __exit__(self, exc_type, exc_val, exc_tb):
		self.mutex.release()
	
	def __init__(self, onChange):
		self.mutex = threading.Lock()
		self.id = genid.genId() # generate unique ID
		self.pc = {} # set of PC
		self.onChange = onChange # Callback function on change set of PC
		# Run threads
		self.isContTimer = recv.IsCont()
		self.isContRecv = recv.IsCont()
		self.__thRecv = threading.Thread(target = self.__runRecv) # reciver messages
		self.__thRecv.start()
		self.__thTimer = threading.Thread(target = self.__runTimer) # sender messages & check PC
		self.__thTimer.start()
	
	def stop(self):
		# Stop threads
		with self:
			self.isContTimer.val = False
		self.__thTimer.join()
		# print('Timer removed')
		with self:
			self.isContRecv.val = False
		self.__thRecv.join()
		# print('Reciever removed')
		# Send 'close' message
		self.__send(['close'])
	
	def __cbRecv(self, res, ip):
		# Parse message
		res = res.split(" ")
		if(len(res) < 2):
			return
		id = res[0]
		act = res[1]
		# print("%20s %10s" % (id, act))
		if(act == 'close'):
			with self:
				if not id in self.pc.keys():
					return
				self.pc[id]['state'] = 'closed'
				self.onChange(id)
			return
		if(act == 'alive'):
			with self:
				isChange = not id in self.pc.keys()
				curTime = time.time()
				if(isChange):
					self.pc[id] = {
						'state': 'alive',
						'lastTime': curTime,
						'ip': ip[0],
						'port': ip[1]
					}
				state = self.pc[id].get('state')
				self.pc[id]['state'] = 'alive'
				self.pc[id]['lastTime'] = curTime
				if(isChange or state == 'lost'):
					self.onChange(id)
				pass
			return
		pass
	
	def __cbTimer(self):
		curTime = time.time()
		for key in self.pc.keys():
			state = self.pc[key]['state']
			if(state == 'closed'):
				continue
			lastTime = self.pc[key]['lastTime']
			isDead = (curTime - lastTime) >= params.TIMEOUT_ALIVE
			isChange = False
			if isDead:
				if state != 'lost':
					state = 'lost'
					isChange = True
				pass
			else:
				if state != 'alive':
					state = 'alive'
					isChange = True
				pass
			self.pc[key]['state'] = state
			if(isChange):
				self.onChange(key)
			pass
		pass
	
	def __runRecv(self):
		recv.recv(self.__cbRecv, self.isContRecv)
	
	def __send(self, action):
		action = [self.id] + action
		action = ' '.join(action)
		req.req(action)
	
	def __runTimer(self):
		while self.isContTimer.val:
			self.__send(['alive'])
			try:
				with self:
					self.__cbTimer()
			except KeyError:
				pass
			time.sleep(params.TIMEOUT)
		pass
