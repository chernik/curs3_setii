import work
import params
from os import system
import sys

def main():
	if len(sys.argv) >= 2 and sys.argv[1] != "":
		if sys.argv[1] == "-ipv6":
			params.MCAST_GRP = "ff15:7079:7468:6f6e:6465:6d6f:6d63:6173"
		else:
			params.MCAST_GRP = sys.argv[1]
	w = 0
	def onChange(id):
		system('clear')
		print('Me => %s' % w.id)
		def cD(key):
			str = '  '
			if(key == id):
				str = ' !'
			return str
		for key in w.pc.keys():
			d = w.pc[key]
			print('%20s %15s %10.1f %10s %s' % (key, d['ip'], d['lastTime'], d['state'], cD(key)))
			pass
		pass
	w = work.Work(onChange)
	print('Started!')
	try:
		s = input()
	except:
		pass
	print('\nStopping...')
	w.stop()
	print('All!')

main()
