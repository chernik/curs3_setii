import time
import random

random.seed()

def genId():
	return "%d_%d" % (round(time.time()*1000), random.randrange(256))
