import socket
import struct

import params

def req(data):
	data = bytes(data, "utf-8")
	addrinfo = socket.getaddrinfo(params.MCAST_GRP, None)[0]
	s = socket.socket(addrinfo[0], socket.SOCK_DGRAM)
	ttl_bin = struct.pack('@i', 1)
	if addrinfo[0] == socket.AF_INET:
		s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl_bin)
	else:
		s.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, ttl_bin)
	s.sendto(data, (addrinfo[4][0], params.MCAST_PORT))
	
