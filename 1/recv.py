import socket
import struct

import params

class IsCont:
	val = True

def recv(cb, isCont):
	addrinfo = socket.getaddrinfo(params.MCAST_GRP, None)[0]
	s = socket.socket(addrinfo[0], socket.SOCK_DGRAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.bind(('', params.MCAST_PORT))
	group_bin = socket.inet_pton(addrinfo[0], addrinfo[4][0])
	if addrinfo[0] == socket.AF_INET:
        	mreq = group_bin + struct.pack('=I', socket.INADDR_ANY)
        	s.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
	else:
		mreq = group_bin + struct.pack('@I', 0)
		s.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mreq)
	s.settimeout(params.TIMEOUT)
	while True:
		res = None
		ip = None
		try:
			res, ip = s.recvfrom(1024)
		except KeyboardInterrupt:
			print('\nKeyboard?')
		except socket.timeout:
			pass
		except Exception as e:
			print('Exception')
			print(e)
			pass
		if(not isCont.val):
			break
		if(res == None):
			continue
		res = str(res, params.STRCODE)
		cb(res, ip)
	pass
