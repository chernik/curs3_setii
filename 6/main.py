import socket, select

BUF_LEN = 1024 * 1024
DEST_ADDR = ('fit.ippolitov.me', 80)
# DEST_ADDR = ('192.168.132.130', 8001)

def bind():
	s = socket.socket()
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	p = 9000
	while True:
		try:
			s.bind(('', p))
		except:
			p += 1
			continue
		break
	print('Binding on port > %d' % p)
	s.listen(100)
	s.setblocking(0)
	return s

def accept(gs):
	s, addr = gs.accept()
	print('Connected > %s [%d]' % (str(addr), s.fileno()))
	s.setblocking(0)
	return s

def newcon(destAddr):
	s = socket.socket()
	s.setblocking(0) # TODO: remove comment
	try:
		s.connect(destAddr)
	except OSError as e:
		if e.errno != 115: # [Errno 115] Operation now in progress
			print(e)
			return None
	except Exception as e:
		print(e)
		return None
	print('Connecting > %s [%d]' % (str(DEST_ADDR), s.fileno()))
	return s

def read(rs, len):
	return rs.recv(len)

def write(ws, data):
	return ws.send(data)

def start(destAddr):
	gs = bind() # listener
	inps = [gs] # sockets for read
	outs = [] # sockets for write
	qs = {}
	while True:
		try:
			rss, wss, ess = select.select(inps, outs, inps + outs) # select
		except KeyboardInterrupt:
			print('')
			break
		"""
		except ValueError:
			rems = []
			for s in inps + outs:
				if s.fileno() == -1 and not s in rems:
					rems.append(s)
			for s in rems:
				if s in inps:
					inps.remove(s)
				else:
					outs.remove(s)
			continue
		"""
		for rs in rss:
			if rs is gs: # listener
				rs = accept(gs) # get client socket
				ws = newcon(destAddr) # get server socket
				if ws == None:
					rs.close()
					continue
				inps.append(rs) # wait data from both sides
				inps.append(ws)
				qs[rs] = { # client => server
					'q': b'',
					'rs': rs,
					'ws': ws,
					'isClosed': False
				}
				qs[ws] = { # server => client
					'q': b'',
					'rs': ws,
					'ws': rs,
					'isClosed': False
				}
				continue # next
			q = qs[rs] # get queue
			try:
				rb = read(rs, BUF_LEN - len(q['q'])) # read data
			except OSError as e:
				if not rs in ess:
					ess.append(rs)
				continue
			if len(rb) == 0: # stream closed
				inps.remove(rs) # noth to read
				q['isClosed'] = True # mark closed
			else: # some data
				q['q'] += rb # save data
				if len(q['q']) == BUF_LEN: # full buffer
					inps.remove(rs) # cannot read data
			if not q['ws'] in outs: # we can write data
				outs.append(q['ws'])
		for ws in wss:
			q = qs[qs[ws]['ws']] # get queue
			if len(q['q']) > 0: # has buffer
				try:
					writed = write(ws, q['q']) # write buffer
				except OSError:
					if not ws in ess:
						ess.append(ws)
				if writed == 0:
					print('Writed = 0')
					continue
				q['q'] = q['q'][writed:] # remove writed
				if not q['rs'] in inps and not q['isClosed']: # we can read again
					inps.append(q['rs'])
				if len(q['q']) == 0 and not q['isClosed']: # no buffer and no close
					outs.remove(ws) # nothing to write
				continue
			if q['isClosed']: # closed
				try:
					ws.shutdown(socket.SHUT_WR)
				except OSError:
					print('Cannot shutdown')
				outs.remove(ws) # cannot write
				if qs[ws]['isClosed'] and not q['rs'] in outs: # both closed
					qs.pop(q['rs'])
					qs.pop(ws)
					q['rs'].close()
					ws.close()
		for es in ess: # errors
			print('Error [%d]' % es.fileno())
			q = qs[es]
			q['rs'].close()
			q['ws'].close()
			qs.pop(q['rs'])
			qs.pop(q['ws'])
			if q['rs'] in inps:
				inps.remove(q['rs'])
			if q['ws'] in inps:
				inps.remove(q['ws'])
			if q['rs'] in outs:
				outs.remove(q['rs'])
			if q['ws'] in outs:
				outs.remove(q['ws'])
	gs.close()
	dupl = []
	print('VVVVVVVVVVV')
	for q in qs:
		if qs[q] in dupl:
			continue
		dupl.append(qs[q])
		print('%d %d' % (qs[q]['rs'].fileno(), qs[q]['ws'].fileno()))
	print('^^^^^^^^^^^')

start(DEST_ADDR)
