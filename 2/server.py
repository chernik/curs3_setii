from conn import Server, ConnectError
import sys
from multiprocessing.dummy import Pool
from traceback import print_tb

def worker(s):
	try:
		Server.goSocket(s)
	except ConnectError as e:
		print('Server error > ' + e.message)
	except Exception as e:
		print_tb(e)

def main():
	if len(sys.argv) < 3:
		ip = '0.0.0.0'
		port = 12345
	else:
		ip = sys.argv[1]
		try:
			port = int(sys.argv[2])
		except ValueError:
			print('Wrong port')
			return
	print('Server binding at (%s %d)...' % (ip, port))
	server = Server(ip, port, 1)
	thrs = 4
	try:
		server.bind(thrs)
	except ConnectError as e:
		print("Server error > " + e.message)
		return
	pool = Pool(thrs)
	try:
		while True:
			try:
				s, ip = server.getSocket()
			except ConnectError as e:
				print('Server error > ' + e.message)
				continue
			if s == None:
				continue
			print('Server recived from ' + str(ip))
			pool.apply_async(worker, (s,))
	except KeyboardInterrupt:
		print('\nClosed')
	server.exit()

main()