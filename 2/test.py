import socket
import sys

def recv():
	s = socket.socket()
	s.bind(("", 12345))
	s.listen(10)
	conn, arrd = s.accept()
	conn.settimeout(1)
	while True:
		try:
			data = conn.recv(256)
		except socket.timeout:
			print("server: timeout")
			break
		except ConnectionResetError:
			print("server: reset connection")
			break
		if data == None:
			print("no data")
			return
		data = data.decode("utf-8")
		data = "\nRecv -----------------------------\n" % data
		print(data)
		data = data.encode("utf-8")
		conn.send(data)
	conn.close()

def send():
	s = socket.socket()
	try:
		s.connect(("127.0.0.1", 12345))
	except ConnectionRefusedError:
		print("Cannot connect")
		return
	data = "hello"
	data = data.encode("utf-8")
	s.send(data)
	s.settimeout(1)
	data = s.recv(256)
	if data == None:
		print("No data")
		return
	data = data.decode("utf-8")
	print("Recieve > %s" % data)
	s.close()

if sys.argv[1] == "-client":
	send()
else:
	recv()
pass