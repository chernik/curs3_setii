from conn import Client, ConnectError
import sys

def main():
	if len(sys.argv) < 2:
		print('<prog> <file> [<ip> <port>]')
		return
	file = sys.argv[1]
	if len(sys.argv) < 4:
		ip = '127.0.0.1'
		port = 12345
	else:
		ip = sys.argv[2]
		try:
			port = int(sys.argv[3])
		except ValueError:
			print('Wrong port')
			return
		pass
	client = Client(ip, port, 1)
	try:
		res = "Response > " + client.send(file)
	except ConnectError as e:
		res = "Error > " + e.message
	print(res)

main()