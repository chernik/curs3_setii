from django.shortcuts import render
import os
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from . import models
import time

# Create your views here.

# ------------------------------------------------------ Files

def main(request):
	return render(request, 'index.htm')

def file(request):
	f = request.path.split('/')[-1]
	return render(request, f)

# ----------------------------------------------------------- Shared functions

def packErr(err):
	resp = HttpResponse()
	resp.status_code = err
	return resp

def checkPost(request): # Check POST request with JSON
	if request.META['CONTENT_TYPE'] != 'application/json' or request.method != 'POST':
		return packErr(400)
	return None

def packJson(doc): # return response with JSON
	resp = HttpResponse(json.dumps(doc).encode('utf-8'))
	resp['Content-Type'] = 'application/json'
	resp.status_code = 200
	return resp

def unpackJson(request):
	return json.loads(request.body.decode('utf-8'))

def getSession(request):
	try:
		session_id = int(request.META['HTTP_AUTHORIZATION'])
	except KeyError:
		return (None, packErr(400))
	session = models.Session.objects.all().filter(id = session_id)
	if session.count() == 0:
		return (None, packErr(401))
	return (session, None)

def getUserId(request):
	session, resp = getSession(request)
	if resp != None:
		return (None, resp)
	user_id = session.values('user_id')[0]['user_id']
	user = models.User.objects.all().filter(id = user_id)[0]
	user.lastTime = time.time()
	user.save()
	return (user_id, None)

# ------------------------------------------------------------------- Listeners

@csrf_exempt
def login(request):
	resp = checkPost(request)
	if resp != None:
		return resp
	doc = unpackJson(request)
	username = doc['username']
	users = models.User.objects.all().filter(username = username, online = models.ONLINE)
	if users.count() > 0:
		resp = packErr(401)
		resp['WWW-Authenticate'] = 'Username is already in use'
		return resp
	user = models.User(
		username = username,
		online = models.ONLINE,
		lastTime = time.time()
	)
	user.save()
	session = models.Session(
		user_id = user.id
	)
	session.save()
	return packJson({
		'id': user.id,
		'username': username,
		'online': user.online,
		'token': session.id
	})

@csrf_exempt
def logout(request):
	resp = checkPost(request)
	if resp != None:
		return resp
	session, resp = getSession(request)
	if resp != None:
		return resp
	user_id = session.values('user_id')[0]['user_id']
	session.delete()
	user = models.User.objects.all().filter(id = user_id)[0]
	user.online = models.OFFLINE
	user.save()
	return packJson({
		'message': 'bye!'
	})

def users(request):
	user_id, resp = getUserId(request)
	if resp != None:
		return resp
	users = models.User.objects.all().values('id', 'username', 'online')
	return packJson({
		'users': list(users)
	})

def user(request, other_user_id):
	user_id, resp = getUserId(request)
	if resp != None:
		return resp
	other_user = models.User.objects.all()
	other_user = other_user.filter(id = other_user_id)
	if other_user.count() == 0:
		return packErr(404)
	other_user = other_user.values('id', 'username', 'online')[0]
	return packJson(other_user)

@csrf_exempt
def messages(request):
	if request.method == 'GET':
		user_id, resp = getUserId(request)
		if resp != None:
			return resp
		try:
			offset = int(request.GET['offset'])
			count = int(request.GET['count'])
		except KeyError:
			return packErr(400)
		mes = models.Message.objects.all()
		mes = list(mes.filter(id__gte = offset)[:count].values('id', 'user_id', 'message', 'postTime'))
		return packJson({
			'messages': mes
		})
	resp = checkPost(request)
	if resp != None:
		return resp
	user_id, resp = getUserId(request)
	if resp != None:
		return resp
	doc = unpackJson(request)
	text = doc['message']
	postTime = time.time()
	print('%s %s %s' % (user_id, text, postTime))
	mes = models.Message(
		user_id = user_id,
		message = text,
		postTime = postTime
	)
	mes.save()
	return packJson({
		'message': text,
		'id': mes.id
	})
