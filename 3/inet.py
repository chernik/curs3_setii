from meta import Meta
import json, time
from conn import Connecter
from threading import Thread, Lock, currentThread
from meta import log
import log

class State:
	def __init__(self, state = False):
		self.__state = state
		self.__mutex = Lock()
	
	def __ml(self):
		self.__mutex.acquire()
	
	def __mu(self):
		self.__mutex.release()
	
	def setState(self, state):
		self.__ml()
		self.__state = state
		self.__mu()
	
	def getState(self):
		self.__ml()
		state = self.__state
		self.__mu()
		return state

def getTimerTh(interval, state, function, args = (), kwargs = {}):
	def funcW():
		function(*args, **kwargs)
	def funcT():
		while not state.getState():
			time.sleep(interval)
			funcW()
		# log('end timer')
	return Thread(target = funcT)

class Inet:
	def __init__(self, myNode, name = 'alien', parentNode = None):
		self.__conn = Connecter(**myNode)
		self.__name = name
		self.__meta = Meta(
			self.__sender,
			self.__reciever,
			self.__mather,
			myNode,
			parentNode
		)
		self.__meta.init()
		self.__thRecv = Thread(target = self.__meta.listen)
		self.__state = State()
		self.__thTimer = getTimerTh(
			interval = self.__meta.getTimeoutForTimer(),
			state = self.__state,
			function = self.__meta.onTimer
		)
	
	def __log(self, mes, s):
		if mes['type'] == 'normal':
			log.log('On %10s %5s [%s] %s => %s (%s) (%s) (%s)' % (
				self.__name,
				s,
				mes['id'],
				mes['data']['node']['port'],
				mes['data']['dest']['port'],
				mes['data'].get('data'),
				mes['data'].get('action'),
				mes['data'].get('isMulticast')
			))
		else:
			log.log('On %10s %5s accept for [%s]' % (
				self.__name,
				s,
				mes['idMes']
			))
	
	def __sender(self, mes, node):
		self.__log(mes, 'send')
		mes = json.dumps(mes)
		self.__conn.send(mes, node)
	
	def __reciever(self, recvCB):
		def cb(mes, node):
			if mes != None:
				mes = json.loads(mes)
				self.__log(mes, 'recv')
				recvCB(mes, node)
			return self.__state.getState()
		self.__conn.cbrecv(cb)
		# log('end recv')
	
	def __mather(self, data):
		print('> Recv <%s> from %s > %s\n~ ' % (
			data['type'],
			self.__name,
			str(data['data'])
		), end = '')
	
	def listenAsync(self):
		self.__thRecv.start()
		self.__thTimer.start()
	
	def send(self, data):
		self.__meta.send(data)
	
	def asRoot(self):
		self.__meta.asRoot()
	
	def logMes(self):
		log.log('---------- From %s' % self.__name)
		self.__meta.logMes()
	
	def goAway(self):
		self.__meta.goAway()
	
	def stop(self):
		self.goAway()
		self.__state.setState(True)
		self.__thRecv.join()
		self.__thTimer.join()

class Nodes:
	def __init__(self, parentPort, parentName = 'parent'):
		self.__nodes = {}
		self.__parent = {
			'port': parentPort
		}
		inet = Inet(
			self.__parent,
			name = parentName
		)
		inet.listenAsync()
		self.__nodes[parentName] = inet
	
	def newNode(self, port, name, parentPort = None):
		if parentPort == None:
			parentNode = self.__parent
		else:
			parentNode = {
				'port': parentPort
			}
		inet = Inet(
			{'port': port},
			name = name,
			parentNode = parentNode
		)
		inet.listenAsync()
		self.__nodes[name] = inet
	
	def newRoot(self, name):
		self.__nodes[name].asRoot()
	
	def go(self, name, data):
		self.__nodes[name].send(data)
	
	def logMes(self):
		for key in self.__nodes.keys():
			self.__nodes[key].logMes()
	
	def stop(self):
		for key in self.__nodes.keys():
			self.__nodes[key].stop()

def test():
	nodes = Nodes(3333)
	def logMes():
		time.sleep(2)
		nodes.logMes()
	logMes()
	nodes.newNode(3334, 'child')
	logMes()
	nodes.go('child', 'hi')
	logMes()
	nodes.newNode(3335, 'child2')
	logMes()
	nodes.go('child', 'hi2')
	logMes()
	nodes.newNode(3336, 'child3', 3334)
	logMes()
	nodes.go('child3', 'hi3')
	logMes()
	nodes.newRoot('child')
	logMes()
	nodes.go('parent', 'hello1')
	logMes()
	nodes.go('child3', 'hallo2')
	logMes()
	print('|||||||||||||||||||||||| end', flush = 'True')
	try:
		while True:
			logMes()
	except KeyboardInterrupt:
		print('')
	print('Stopping...')
	nodes.stop()
# test()