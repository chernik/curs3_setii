import uuid, time
from threading import Lock

"""
Класс, представляющий узел дерева
Нода - необходимые данные для отправки, например, ip
__init__: parentNode - нода родителя
onMessage - обработка сообщения mes (формата Tree) с ноды node, возвращает ответ (None - ответ не тредуется) и данные (None - нету данных)
proadcastMessage - отправка всем узлам данных data, возвращает массив {node: нода, data: данные формата Tree}
"""

class Tree:
	def __init__(self, myNode, parentNode = None):
		self.__parent = {
			'id': '',
			'node': parentNode,
			'time': time.time()
		}
		self.__myNode = myNode
		if parentNode == None:
			self.__parent['id'] = None
		self.__childs = {}
		self.__id = str(uuid.uuid4())
		self.__mutex = Lock()
	
	def __ml(self):
		self.__mutex.acquire()
	
	def __mu(self):
		self.__mutex.release()
	
	def onMessage(self, mes, node):
		self.__ml()
		sender = mes['sender']
		if mes.get('action') == 'imParent':
			if self.__parent['id'] == '':
				self.__parent['id'] = sender
				self.__parent['time'] = time.time()
		res = ([], mes.get('data'))
		if self.__parent['id'] == '':
			self.__mu()
			return res
		if sender == self.__parent['id']:
			self.__parent['time'] = time.time()
			if mes.get('action') == 'bye':
				self.__mu()
				self.detachParent()
				res[0].extend(self.broadcastMessage(res[1]))
				return res
		elif sender in self.__childs.keys():
			child = self.__childs[sender]
			child['node'] = mes['node']
			child['time'] = time.time()
			if mes.get('action') == 'bye':
				self.__childs.pop(sender)
				self.__mu()
				res[0].extend(self.broadcastMessage(res[1]))
				return res
		else:
			if mes.get('action') == 'bye':
				return res
			child = {
				'id': sender,
				'node': mes['node'],
				'time': time.time()
			}
			self.__childs[sender] = child
			acc = {
				'sender': self.__id,
				'action': 'imParent',
				'node': self.__myNode,
				'dest': mes['node']
			}
			res[0].append({
				'data': acc,
				'node': mes['node']
			})
		self.__mu()
		if mes.get('isMulticast'):
			res[0].extend(self.broadcastMessage(res[1], sender))
		return res
	
	def broadcastMessage(self, data, exclude = None):
		self.__ml()
		arr = []
		if self.__parent['id'] != None and self.__parent['id'] != exclude:
			arr.append({
				'node': self.__parent['node'],
				'data': {
					'sender': self.__id,
					'isMulticast': True,
					'data': data,
					'node': self.__myNode,
					'dest': self.__parent['node']
				}
			})
		for key in self.__childs.keys():
			if key == exclude:
				continue
			child = self.__childs[key]
			arr.append({
				'node': child['node'],
				'data': {
					'sender': self.__id,
					'isMulticast': True,
					'data': data,
					'node': self.__myNode,
					'dest': child['node']
				}
			})
		self.__mu()
		return arr
	
	def attachParent(self):
		if self.__parent['id'] == None:
			return None
		return {
			'node': self.__parent['node'],
			'data': {
				'sender': self.__id,
				'node': self.__myNode,
				'dest': self.__parent['node']
			}
		}
	
	def detachParent(self):
		self.__ml()
		self.__parent = {
			'node': None,
			'id': None
		}
		self.__mu()
	
	def removeTimeouted(self, timeout):
		self.__ml()
		t = time.time()
		res = False
		for key in self.__childs.keys():
			child = self.__childs[key]
			if (t - child['time']) >= timeout:
				self.__childs.pop(key)
				break
		if self.__parent['id'] != None and (t - self.__parent['time']) >= timeout:
			res = True
		self.__mu()
		res = False
		if res:
			self.detachParent()
		return res
	
	def goAway(self):
		res = self.broadcastMessage(None)
		for i in range(len(res)):
			res[i]['data']['isMulticast'] = None
			res[i]['data']['action'] = 'bye'
		return res
