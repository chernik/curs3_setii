import tree, data, threading, log

def log(mes):
	log.log('%s %s' % (mes, str(threading.currentThread())), flush = True)

class Meta:
	def __init__(self,
			sender, reciever, mather,
			myNode, parentNode = None,
			mesTO = 1, oldMesTO = 30, nodeTO = 5*60 ):
		self.__sender = sender
		self.__reciever = reciever
		self.__mather = mather
		self.__tree = tree.Tree(myNode, parentNode)
		self.__mess = data.Data()
		self.__tos = [mesTO, oldMesTO, nodeTO]
		if parentNode == None:
			self.__mess.newHash()
	
	def init(self):
		md = self.__tree.attachParent()
		if md == None:
			return
		mes = self.__mess.raiseMes(md['data'])
		self.__sender(mes, md['node'])
	
	def __recieverCB(self, mes, node):
		res, md = self.__mess.addMes(mes)
		if res != None:
			self.__sender(res, mes['data']['node'])
		if md == None:
			return
		res, data = self.__tree.onMessage(md, node)
		isBroadcastHash = mes.get('newHash') and mes['data'].get('isMulticast')
		for gp in res:
			md = self.__mess.raiseMes(gp['data'], gp['data'].get('action') == 'imParent' or isBroadcastHash)
			self.__sender(md, gp['node'])
		if data != None:
			self.__mather({
				'type': 'new',
				'data': data
			})
	
	def listen(self):
		self.__reciever(self.__recieverCB)
	
	def send(self, data, newHash = None):
		mds = self.__tree.broadcastMessage(data)
		for md in mds:
			mes = self.__mess.raiseMes(md['data'], newHash)
			self.__sender(mes, md['node'])
	
	def asRoot(self):
		self.__tree.detachParent()
		self.__mess.newHash()
		self.send(None, True)
	
	def logMes(self):
		self.__mess.logMes()
	
	def getTimeoutForTimer(self):
		return min(self.__tos)
	
	def onTimer(self):
		mes = self.__mess.getTimeouted(self.__tos[1], 'normal')
		for m in mes:
			self.__mess.rmMes(m)
			self.__mather({
				'type': 'remove',
				'data': m['data']['data']
			})
		mes = self.__mess.getTimeouted(self.__tos[0], 'normal')
		for m in mes:
			# self.__mess.clearTime(m)
			self.__sender(m, m['data']['dest'])
		mes = self.__mess.getTimeouted(self.__tos[1], 'recieved')
		for m in mes:
			self.__mess.rmMes(m)
		if self.__tree.removeTimeouted(self.__tos[2]):
			self.__mather({
				'type': 'system',
				'data': 'New root'
			})
			self.asRoot()
	
	def goAway(self):
		mds = self.__tree.goAway()
		for md in mds:
			mes = self.__mess.raiseMes(md['data'], once = True)
			self.__sender(mes, md['node'])

class Node:
	def __init__(self, nodes, id, parentNode = None):
		self.__nodes = nodes
		self.__id = id
		self.__recvCB = None
		self.__meta = Meta(
			self.__sender,
			self.__reciever,
			self.__mather,
			id,
			parentNode
		)
	
	def init(self):
		self.__meta.listen()
		self.__meta.init()
	
	def __sender(self, mes, node):
		# print('%s > %s > %s' % (self.__id, mes, node))
		self.__nodes[node].onSend(mes, self.__id)
	
	def __reciever(self, recvCB):
		self.__recvCB = recvCB
	
	def __mather(self, data):
		print('From %s >' % self.__id)
		print(data)
		print('----------------------')
	
	def go(self, data):
		print('From %s sending...' % self.__id)
		print(data)
		print('----------------------')
		self.__meta.send(data)
	
	def onSend(self, mes, node):
		if self.__recvCB != None:
			self.__recvCB(mes, node)
	
	def asRoot(self):
		self.__meta.asRoot()
	
	def logMes(self):
		print('>>>>>> From %s' % self.__id)
		self.__meta.logMes()

class Nodes:
	def __init__(self, parentName = 'parent'):
		self.__parent = parentName
		self.__nodes = {}
		self.__nodes[parentName] = Node(
			self.__nodes,
			parentName
		)
		self.__nodes[parentName].init()
	
	def newNode(self, name, parent = None):
		if parent == None:
			parent = self.__parent
		self.__nodes[name] = Node(
			self.__nodes,
			name,
			parent
		)
		self.__nodes[name].init()
	
	def newRoot(self, name):
		self.__nodes[name].asRoot()
	
	def go(self, name, data):
		self.__nodes[name].go(data)
	
	def logMes(self):
		for key in self.__nodes.keys():
			self.__nodes[key].logMes()

def test():
	nodes = Nodes()
	nodes.logMes()
	nodes.newNode('child')
	nodes.logMes()
	nodes.go('child', 'hi')
	nodes.logMes()
	nodes.newNode('child2')
	nodes.logMes()
	nodes.go('child', 'hi2')
	nodes.logMes()
	nodes.newNode('child3', 'child')
	nodes.logMes()
	nodes.go('child3', 'hi3')
	nodes.logMes()
	nodes.newRoot('child')
	nodes.logMes()
	nodes.go('parent', 'hello1')
	nodes.logMes()
	nodes.go('child3', 'hallo2')
	nodes.logMes()

# test()