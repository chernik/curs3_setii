from sys import argv
from inet import Inet
from cmd import Cmd

class MyCmd(Cmd):
	def __init__(self, inet):
		self.__inet = inet
		super(MyCmd, self).__init__()
		self.prompt = '~ '
	
	def precmd(self, line):
		if line == '':
			return 'empty'
		if line[0:1] == '/':
			line = line[1:]
			try:
				getattr(self.__inet, line)()
			except AttributeError:
				return line
			if line == 'stop':
				return line
		else:
			self.__inet.send(line)
		return 'empty'
	
	def do_stop(self, line):
		return True
	
	def do_empty(self, line):
		pass
	
	def do_help(self, line):
		print('------------ help\n\t', end = '')
		print('\n\t'.join(filter(
			lambda el: el.find('_') == -1,
			dir(self.__inet)
		)))
		print('----------- end help')

def main():
	args = dict(map(lambda x: x.lstrip('-').split('='), argv[1:]))
	def addDef(key, val):
		if args.get(key) == None:
			args[key] = val
	addDef('ip', '127.0.0.1')
	addDef('Pip', '127.0.0.1')
	addDef('lost', '0')
	try:
		myNode = {
			'ip': args['ip'],
			'port': int(args['p']),
			'lost': int(args['lost'])
		}
		if args.get('Pp') != None:
			parentNode = {
				'ip': args['Pip'],
				'port': int(args['Pp'])
			}
		else:
			parentNode = None
	except KeyError:
		print('<prog> p=<myPort> [ip=<myIP>] [Pp=<parentPort> [Pip=<parentIp>]] [lost=<% lost messages>]')
		return
	except ValueError:
		print('<myPort> and <parentPort> must be INT')
		return
	inet = Inet(myNode = myNode, parentNode = parentNode)
	inet.listenAsync()
	cmd = MyCmd(inet)
	cmd.cmdloop()

main()
