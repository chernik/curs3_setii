from threading import Lock
import time, random, json, uuid, log

class Data:
	random.seed()
	
	def __init__(self):
		self.__queue = {}
		self.__mutex = Lock()
		self.__hash = ''
	
	def __ml(self):
		self.__mutex.acquire()
	
	def __mu(self):
		self.__mutex.release()
	
	def __getUniqId(self):
		while True:
			# id = str(time.time()) + '_' + str(random.randrange(0,100))
			id = str(uuid.uuid4())
			if id in self.__queue.keys():
				continue
			break
		return id
	
	def newHash(self):
		self.__ml()
		self.__hash = str(uuid.uuid4())
		self.__mu()
	
	def raiseMes(self, data, newHash = False, once = False):
		self.__ml()
		mes = {
			'id': self.__getUniqId(),
			'data': data,
			'type': 'normal',
			'time': time.time(),
			'hash': self.__hash
		}
		if newHash:
			mes['newHash'] = True
		if not once:
			self.__queue[mes['id']] = mes
		self.__mu()
		return mes
	
	def getTimeouted(self, timeout, type):
		self.__ml()
		t = time.time()
		res = []
		for key in self.__queue.keys():
			mes = self.__queue[key]
			if mes['type'] != type:
				continue
			if (t - mes['time']) >= timeout:
				mes['hash'] = self.__hash
				res.append(mes)
				continue
		self.__mu()
		return res
	
	def clearTime(self, mes):
		self.__queue[mes['id']]['time'] = time.time()
	
	def addMes(self, mes):
		self.__ml()
		res = (None, None)
		if mes.get('newHash'):
			log.log('[%s] => [%s]' % (mes['hash'], self.__hash))
		else:
			log.log('[%s] <! [%s]' % (mes['hash'], self.__hash))
		if mes['hash'] == '':
			mes['hash'] = self.__hash
		if self.__hash == '':
			# self.__hash = mes['hash']
			pass
		isRet = False
		if mes.get('newHash') != None:
			self.__hash = mes['hash']
		if self.__hash != mes['hash']:
			isRet = True
		if isRet:
			self.__mu()
			return res
		if mes['type'] == 'accept':
			idMes = mes['idMes']
			rmMes = idMes
			try:
				rmMes = self.__queue.pop(idMes)
			except KeyError:
				pass
		elif mes['type'] == 'normal':
			accept = {
				'id': 'magic',
				'type': 'accept',
				'idMes': mes['id'],
				'hash': self.__hash
			}
			oldMes = self.__queue.get(mes['id'])
			if oldMes == None:
				res = (accept, mes['data'])
				oldMes = {
					'id': mes['id'],
					'type': 'recieved',
					'time': time.time()
				}
				self.__queue[mes['id']] = oldMes
			else:
				res = (accept, None)
				oldMes['time'] = time.time()
		self.__mu()
		return res
	
	def rmMes(self, mes):
		self.__ml()
		try:
			self.__queue.pop(mes['id'])
		except KeyError:
			pass
		self.__mu()
	
	def logMes(self):
		self.__ml()
		for key in self.__queue.keys():
			mes = self.__queue[key]
			if mes['type'] == 'normal':
				data = mes.get('data')
			elif mes['type'] == 'recieved':
				data = ''
			print('\t%s %10s %s' % (mes['id'], mes['type'], data))
		self.__mu()
		print('-------------- end log')
	
	def pack(mes):
		return json.dumps(mes)
	
	def unpack(s):
		return json.loads(s)
