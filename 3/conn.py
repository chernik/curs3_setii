import socket
import random, log

class Connecter:
	def __init__(self,
			ip = '127.0.0.1',
			port = 3333,
			size = 1024,
			lost = 0,
			timeout = 1 ):
		self.__ip = ip
		self.__port = port
		self.__size = size
		self.__lost = lost
		self.__timeout = timeout
		random.seed()
	
	def send(self, data, node):
		data = data.encode('utf-8')
		s = socket.socket(
			socket.AF_INET,
			socket.SOCK_DGRAM
		)
		nodel = [self.__ip, self.__port]
		if node.get('ip') != None:
			nodel[0] = node['ip']
		if node.get('port') != None:
			nodel[1] = node['port']
		nodel = tuple(nodel)
		s.sendto(data, nodel)
	
	def bind(self):
		s = socket.socket(
			socket.AF_INET,
			socket.SOCK_DGRAM
		)
		nodel = (self.__ip, self.__port)
		s.bind(nodel)
		log.log('> Bind %s' % (nodel,))
		s.settimeout(self.__timeout)
		return s
	
	def __recv(self, s):
		try:
			data, ip = s.recvfrom(self.__size)
		except socket.timeout:
			return (None, None)
		if random.randrange(0,100) < self.__lost:
			print('$', end = '', flush = True)
			return (None, None)
		data = data.decode('utf-8')
		return (data, ip)
	
	def cbrecv(self, cb):
		s = self.bind()
		while True:
			data, ip = self.__recv(s)
			if data == None:
				if cb(data, ip):
					break
				continue
			ip = {
				'ip': ip[0],
				'port': ip[1]
			}
			if cb(data, ip):
				break
		s.close()
