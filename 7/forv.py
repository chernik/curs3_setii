import cls, socket

def read(rs, len):
	return rs.recv(len)

def write(ws, data):
	return ws.send(data)

class Forv:
	def __init__(self, destAddr, port, rot):
		self.__destAddr = destAddr
		self.__port = port
		self.__qs = {}
		self.__rot = rot
	
	def bind(self):
		self.__s, port = cls.bind(self.__port)
		self.__inps = [self.__s]
		self.__outs = []
		self.__rot.addCl(self.__inps, self.__outs, self.__handler)
		return port
	
	def newCon(self, rs):
		ws = cls.newcon(self.__destAddr) # get server socket
		if ws == None:
			rs.close()
			return
		self.__inps.append(rs) # wait data from both sides
		self.__inps.append(ws)
		qs = self.__qs
		qs[rs] = { # client => server
			'q': b'',
			'rs': rs,
			'ws': ws,
			'isClosed': False
		}
		qs[ws] = { # server => client
			'q': b'',
			'rs': ws,
			'ws': rs,
			'isClosed': False
		}
	
	def __handler(self, inps, outs, rss, wss, ess, handler):
		qs = self.__qs
		gs = self.__s
		for rs in rss:
			if rs is gs: # listener
				rs = cls.accept(gs) # get client socket
				self.newCon(rs)
				continue # next
			q = qs[rs] # get queue
			try:
				rb = read(rs, cls.BUF_LEN - len(q['q'])) # read data
			except OSError as e:
				if not rs in ess:
					ess.append(rs)
				continue
			if len(rb) == 0: # stream closed
				inps.remove(rs) # noth to read
				q['isClosed'] = True # mark closed
			else: # some data
				q['q'] += rb # save data
				if len(q['q']) == cls.BUF_LEN: # full buffer
					inps.remove(rs) # cannot read data
			if not q['ws'] in outs: # we can write data
				outs.append(q['ws'])
		for ws in wss:
			q = qs[qs[ws]['ws']] # get queue
			if len(q['q']) > 0: # has buffer
				try:
					writed = write(ws, q['q']) # write buffer
				except OSError:
					if not ws in ess:
						ess.append(ws)
					continue
				if writed == 0:
					print('Writed = 0')
					continue
				q['q'] = q['q'][writed:] # remove writed
				if not q['rs'] in inps and not q['isClosed']: # we can read again
					inps.append(q['rs'])
				if len(q['q']) == 0 and not q['isClosed']: # no buffer and no close
					outs.remove(ws) # nothing to write
				continue
			if q['isClosed']: # closed
				try:
					ws.shutdown(socket.SHUT_WR)
				except OSError:
					print('Cannot shutdown')
				outs.remove(ws) # cannot write
				if qs[ws]['isClosed'] and not q['rs'] in outs: # both closed
					qs.pop(q['rs'])
					qs.pop(ws)
					q['rs'].close()
					ws.close()
		for es in ess: # errors
			print('Error [%d]' % es.fileno())
			q = qs[es]
			q['rs'].close()
			q['ws'].close()
			qs.pop(q['rs'])
			qs.pop(q['ws'])
			if q['rs'] in inps:
				inps.remove(q['rs'])
			if q['ws'] in inps:
				inps.remove(q['ws'])
			if q['rs'] in outs:
				outs.remove(q['rs'])
			if q['ws'] in outs:
				outs.remove(q['ws'])
	
	def close(self):
		gs = self.__s
		qs = self.__qs
		gs.close()
		dupl = []
		print('VVVVVVVVVVV')
		for q in qs:
			if qs[q] in dupl:
				continue
			dupl.append(qs[q])
			print('%d %d' % (qs[q]['rs'].fileno(), qs[q]['ws'].fileno()))
		print('^^^^^^^^^^^')

def main():
	destAddr = ('fit.ippolitov.me', 80)
	rot = cls.Rotator()
	forv = Forv(destAddr, 9001, rot)
	print('Forv port > %d' % forv.bind())
	while rot():
		pass

# main()
