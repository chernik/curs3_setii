import socket, select

BUF_LEN = 1024 * 1024

class ProtExc(Exception):
	def __init__(self, mess):
		self.mess = mess

class Buffer:
	def __init__(self, s):
		self.__s = s # Socket
		self.__bufIn = b'' # Buffer from socket
		self.__bufOut = b'' # Buffer to socket
		self.__closed = False # Need to close
		self.__nowClosed = False # Closed
		self.__patt = None # Pattern
		self.__data = None # Data
		self.__rw = 'n' # State: n - noth, r - need to read, w - need to write
	
	def goRead(self, *patt): # Set read state
		self.__patt = patt # Save patt
		self.__data = {} # Create data
		self.__rw = 'r' # Set state
	
	def goWrite(self, data, *patt): # Set write state
		if len(self.__bufOut) > 0: # Someth hadn't writted
			raise Exception()
		for el in patt: # Collect binary
			if not el[0] in data: # Wrong name in patt
				raise Exception()
			val = data[el[0]] # Value
			if el[2] == 'n': # Convert from number
				val = val.to_bytes(el[1], byteorder = 'big')
			elif el[2] == 'b': # Convert from byte array
				pass
			else: # Unknown type
				raise Exception()
			self.__bufOut += val # Collect
		self.__rw = 'w' # Set state
	
	def checkRead(self): # Call when socket is ready to read data
		if len(self.__bufIn) >= BUF_LEN: # Full buffer
			return None
		count = 0
		for patt in self.__patt:
			count += patt[1]
		rb = self.__s.recv(min(
			BUF_LEN - len(self.__bufIn),
			count
		)) # Read from socket
		if len(rb) == 0: # Socket is closed
			if len(self.__patt) > 0: # We need more
				raise ProtExc('Unexpected end of data')
			return True # We can call the function
		self.__bufIn += rb # Add readed data
		return self.__checkRead() # Parse readed data
	
	def __checkRead(self):
		while len(self.__patt) > 0 and self.__patt[0][1] <= len(self.__bufIn): # we have patterns & enouf readed data
			if self.__patt[0][0] in self.__data: # Duplicate names
				raise Exception()
			val = self.__bufIn[:self.__patt[0][1]] # Get value
			if self.__patt[0][2] == 'n': # To number
				val = int.from_bytes(val, byteorder = 'big')
			elif self.__patt[0][2] == 'b': # To byte array
				pass
			else: # Unknown type
				raise Exception()
			self.__data[self.__patt[0][0]] = val # Save value
			self.__bufIn = self.__bufIn[self.__patt[0][1]:] # Remove from buffer
			self.__patt = self.__patt[1:] # Remove handled pattern
		return len(self.__patt) == 0 # True if we can call func
	
	def checkWrite(self): # Call when socket is ready to write data
		if len(self.__bufOut) == 0:
			if self.__closed:
				self.__s.close()
				print('--1')
				self.__rw = 'n'
				self.__nowClosed = True
			return True
		writed = self.__s.send(self.__bufOut) # Write
		self.__bufOut = self.__bufOut[writed:] # Remove from buffer
		return len(self.__bufOut) # True if we can call func
	
	def getRW(self):
		return self.__rw
	
	def goClose(self):
		if self.isClosed():
			return
		self.__rw = 'w'
		self.__closed = True
	
	def isClosed(self):
		return self.__nowClosed
	
	def getResult(self):
		return self.__data
	
	def detachSocket(self):
		self.__rw = 'n'
		self.__nowClosed = True
		return self.__s

# ----------------------------------------------------------------------------------

class Rotator:
	def __init__(self):
		self.__cls = []
	
	def addCl(self, inps, outs, handler): # Started inps & outs
		self.__cls.append({
			'inps': inps,
			'outs': outs,
			'handler': handler
		})
	
	def __select(self): # Collector
		inps = []
		outs = []
		for el in self.__cls: # Collect all sockets
			inps += el['inps']
			outs += el['outs']
		try:
			rss, wss, ess = select.select(inps, outs, inps + outs) # Select
		except KeyboardInterrupt:
			return None
		cls = [] # Collect elements for math
		for el in self.__cls:
			for s in rss + wss + ess:
				if s in el['inps'] + el['outs']: # Yeap
					if not el in cls:
						cls.append(el)
						el['rss'] = []
						el['wss'] = []
						el['ess'] = []
					if s in rss and s not in el['rss']:
						el['rss'].append(s)
					if s in wss and s not in el['wss']:
						el['wss'].append(s)
					if s in ess and s not in el['ess']:
						el['ess'].append(s)
		return cls
	
	def select(self): # Iteration
		cls = self.__select()
		if cls == None:
			return False
		for el in cls:
			if el['handler'](**el):
				self.__cls.remove(el)
			for s in el['inps'] + el['outs']:
				if s.fileno() == -1:
					raise Exception()
		return len(self.__cls) != 0
	
	def __call__(self):
		return self.select()

# -----------------------------------------------------------------------------------------------------------

class Protocol:
	def __init__(self, port, rotator, func):
		self.__port = port # port for bind
		self.__qs = {} # queues
		self.__rot = rotator # Rotator
		self.__func = func # Generator of protocol
	
	def bind(self): # Bind
		self.__s = bind(self.__port)[0]
		self.__rot.addCl([self.__s], [], self.__handler)
	
	def __handler(self, inps, outs, rss, wss, ess, handler):
		def handle(y, s):
			rw = y.getRW()
			if rw != 'r':
				if s in inps:
					inps.remove(s)
			else:
				if s not in inps:
					inps.append(s)
			if rw != 'w':
				if s in outs:
					outs.remove(s)
			else:
				if s not in outs:
					outs.append(s)
		def nextIter(sct):
			try:
				y = sct['func'].__next__()
				sct['buffer'] = y
			except StopIteration as e:
				if e.value == None:
					y = sct['buffer']
				else:
					y = e.value
				y.goClose()
			handle(y, sct['s'])
			if y.isClosed():
				self.__qs.pop(sct['s'])
		for rs in rss:
			if rs is self.__s:
				rs = accept(self.__s)
				bf = Buffer(rs)
				sct = {
					'buffer': bf,
					'func': self.__func(bf),
					's': rs
				}
				self.__qs[rs] = sct
				nextIter(sct)
				continue
			sct = self.__qs[rs]
			try:
				if sct['buffer'].checkRead():
					nextIter(sct)
			except ProtExc as e:
				print(e.mess)
				ess.append(rs)
				continue
		for ws in wss:
			sct = self.__qs[ws]
			try:
				if sct['buffer'].checkWrite():
					nextIter(sct)
			except ProtExc as e:
				print(e.mess)
				ess.append(mess)
				continue
		for es in ess:
			print('Err > %s' % str(es))
			es.close()
			if es in inps:
				inps.remove(es)
			if es in outs:
				outs.remove(es)
			if es in self.__qs:
				self.__qs.pop(es)
		return False

# ---------------------------------------------------------------------------------------------------

def bind(p = 0): # Bind on port, 0 for auto select
	s = socket.socket()
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # For reuse
	while True:
		try:
			s.bind(('', p))
		except:
			raise 
		break
	p2 = s.getsockname()[1]
	# print('Binding on port > %d' % p2)
	s.listen(100)
	s.setblocking(0)
	return (s, p2)

def accept(gs):
	s, addr = gs.accept()
	print('Connected > %s [%d]' % (str(addr), s.fileno()))
	s.setblocking(0)
	return s

def newcon(destAddr):
	s = socket.socket()
	s.setblocking(0) # TODO: remove comment
	try:
		s.connect(destAddr)
	except OSError as e:
		if e.errno != 115: # [Errno 115] Operation now in progress
			print(e)
			return None
	except Exception as e:
		print(e)
		return None
	# print('Connecting > %s [%d]' % (str(destAddr), s.fileno()))
	return s
