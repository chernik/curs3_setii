import cls, forv

class Socks:
	def __init__(self, port, rot):
		self.__prot = cls.Protocol(port, rot, self.__handler)
		self.__rot = rot
		self.__conns = {}
	
	def bind(self):
		self.__prot.bind()
	
	def __handler(self, bf):
		def checkV(res):
			if False and res['Version'] != 5:
				raise cls.ProtExc('Wrong version: %d' % res['Version'])
		bf.goRead(
			['Version', 1, 'n'],
			['nM', 1, 'n']
		)
		yield bf
		res = bf.getResult()
		checkV(res)
		bf.goRead(
			['M', res['nM'], 'b']
		)
		yield bf
		res = bf.getResult()
		checkV(res)
		ret = {
			'Version': 5,
			'Method': 0
		}
		closed = False
		if not 0 in list(res['M']):
			ret['Method'] = 0xFF
			print('Do not find auth > %s' % str(list(res['M'])))
			closed = True
		bf.goWrite(ret,
			['Version', 1, 'n'],
			['Method', 1, 'n']
		)
		if closed:
			return bf
		else:
			yield bf
		bf.goRead(
			['Version', 1, 'n'],
			['Cmd', 1, 'n'],
			['Reserved', 1, 'n'],
			['AType', 1, 'n']
		)
		yield bf
		res = bf.getResult()
		checkV(res)
		ret = {
			'Version': 5,
			'Rep': 0,
			'Reserved': 0,
			'AType': 1,
			'addr': bytes([192, 168, 132, 130]),
			'port': 0
		}
		patt = [
			['Version', 1, 'n'],
			['Rep', 1, 'n'],
			['Reserved', 1, 'n'],
			['AType', 1, 'n'],
			['addr', 4, 'b'],
			['port', 2, 'n']
		]
		if res['Cmd'] not in [1, 2]:
			print('Do not find cmd > %d' % res['Cmd'])
			ret['Rep'] = 7
			bf.goWrite(ret, *patt)
			return bf
		if res['AType'] == 1:
			bf.goRead(
				['addr', 4, 'b'],
				['port', 2, 'n']
			)
			yield bf
			addr = bf.getResult()
			addr['addr'] = '.'.join(list(addr['addr']))
		elif res['AType'] == 3:
			bf.goRead(
				['strLen', 1, 'n']
			)
			yield bf
			bf.goRead(
				['addr', bf.getResult()['strLen'], 'b'],
				['port', 2, 'n']
			)
			yield bf
			addr = bf.getResult()
			addr['addr'] = addr['addr'].decode('utf-8')
		else:
			print('Do not support conn type > %d' % res['AType'])
			ret['Rep'] = 8
			bf.goWrite(ret, *patt)
			return bf
		addr = (addr['addr'], addr['port'])
		conn = self.__conns.get(addr)
		if conn == None:
			conn = {}
			self.__conns[addr] = conn
			conn['addr'] = addr
			conn['forv'] = forv.Forv(
				addr, 0, self.__rot
			)
			conn['port'] = conn['forv'].bind()
			print('New conn on %s binded on %d' % (str(addr), conn['port']))
		ret['port'] = conn['port']
		bf.goWrite(ret, *patt)
		if res['Cmd'] == 2:
			return bf
		yield bf
		s = bf.detachSocket()
		conn['forv'].newCon(s)
		return bf
	
	def close(self):
		for key in self.__conns:
			conn = self.__conns[key]
			print('Forvarder from port %d to %s' % (
				conn['port'],
				str(conn['addr'])
			))
			conn['forv'].close()

def main():
	rot = cls.Rotator()
	socks = Socks(9000, rot)
	socks.bind()
	while rot():
		pass
	socks.close()

main()